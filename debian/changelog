ruby-oily-png (1.2.1~dfsg-1) unstable; urgency=medium

  * Update copyright info:
    + Extend coverage of main author.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for Debian packaging.
    + Fix repackage upstream tarball, excluding images with non-free
      ICC profile.
  * Update watch file:
    + Bump to file format 4.
    + Add usage comment.
    + Handle ~dfsg suffix.
  * Modernize git-buildpackage config:
    + Avoid git- prefix.
    + Filter any .git* file.
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.
  * Modernize Vcs-Git field to use https URL.
  * Modernize Vcs-Browser field to use https cgit URL.
  * Modernize CDBS use:
    + Extract metadata from (not skip) png file for copyright check.
    + Tighten build-dependency on cdbs.
    + Build-depend on libregexp-assemble-perl libimage-exiftool-perl
      libfont-ttf-perl.
    + Build-depend on licensecheck (not devscripts).
    + Drop get-orig-source target: Use gbp import-orig --uscan instead.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 14 Dec 2016 15:08:01 +0100

ruby-oily-png (1.1.0-5) unstable; urgency=medium

  * Update Maintainer, Uploaders and Vcs-* fields: Packaging git moved
    to pkg-sass area of Alioth.
  * Extend coverage for myself.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 May 2014 18:39:20 +0200

ruby-oily-png (1.1.0-4) unstable; urgency=medium

  * Fix depend on either (not all) supported Ruby: call
    dh_ruby_fixdepends.
  * Bump standards-version to 3.9.5.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 24 Apr 2014 09:56:48 +0200

ruby-oily-png (1.1.0-3) unstable; urgency=low

  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Stop track md5sum of upstream tarball.
  * Use github as direct source (not via
    pkg-ruby-extras.alioth.debian.org redirector).
  * Bump packaging license to GPL-3+, and extend coverage to include
    current year.
  * Use anonscm.debian.org URL for Vcs-Git.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 06 May 2013 18:34:59 +0200

ruby-oily-png (1.1.0-2) experimental; urgency=low

  * Fix cover PngSuite files in copyright file.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 17 Feb 2013 15:41:49 +0100

ruby-oily-png (1.1.0-1) experimental; urgency=low

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Relax to build-depend unversioned on cdbs and gem2deb: Needed
      versions satisfied in stable, and oldstable no longer supported.
    + Tighten dependency on ruby-chunky-png: Needed since 1.1.0.
  * Have git-import-orig suppress .gitignore files.
  * Fix execute build targets once, by using single-colon stamp targets.
  * Bump dephelper compatibility level to 8.
  * Update copyright file:
    + Fix use pseudo-license and pseudo-comment sections to obey silly
      restrictions of copyright format 1.0.
  * Bump standards-version to 3.9.4.
  * Skip copyright-checking PNGs, and stop have dpkg-source treat
    copyright_hints file as binary.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 17 Feb 2013 14:52:20 +0100

ruby-oily-png (1.0.2-2) unstable; urgency=low

  * Extend copyright coverage of packaging.
  * Bump standards-version to 3.9.3.
  * Bump copyright file format to 1.0.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 26 Jun 2012 21:47:07 +0200

ruby-oily-png (1.0.2-1) unstable; urgency=low

  * New upstream release.
  * Drop dpkg local-options hints from packaging source: now defaults.
  * Update package relations:
    + Tighten dependency on ruby-chunky-png.
    + Relax build-dependency on cdbs (needlessly tight).
    + Relax build-depend unversioned on debhelper and devscripts (needed
      versions satisfied even in oldstable).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Nov 2011 17:13:23 +0700

ruby-oily-png (1.0.1-1) unstable; urgency=low

  * Initial release.
    Closes: bug#632612.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 04 Jul 2011 04:01:44 +0200
